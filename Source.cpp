#include <Windows.h>
#include <iostream>
#include <string>
#include "Helper.h"

#define MAX_PATH 2048

using namespace std;

void main()
{
	TCHAR path[MAX_PATH];
	string command;
	char* tmpCommand;
	vector<string> arguments;
	cout << "> ";
	while (1)
	{
		std::getline(std::cin, command);
		
		Helper::trim(command);
		arguments = Helper::get_words(command);
		
		if (arguments[0].compare("pwd") == 0)
		{
			if (!GetCurrentDirectory(MAX_PATH, path))
			{
				cerr << "Error getting current directory: #" << GetLastError();
			}
			else
			{
				cout << string(path);
			}
		}
		else if (arguments[0].compare("cd") == 0)
		{
			if (!SetCurrentDirectory(arguments[1].c_str()))
			{
				cerr << "Error changing current directory: #" << GetLastError();
			}
		}
		else if (arguments[0].compare("create") == 0)
		{
			if (!CreateFile(arguments[1].c_str(), GENERIC_ALL, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL))
			{
				cerr << "Error creating current file: #" << GetLastError();
			}
		}
		else if (arguments[0].compare("ls") == 0)
		{
			GetCurrentDirectory(MAX_PATH, path);
			WIN32_FIND_DATA data;
			HANDLE hFind = FindFirstFile((string(path) + "\\*").c_str(), &data);      // DIRECTORY

			if (hFind != INVALID_HANDLE_VALUE) {
				do {
					std::cout << data.cFileName << std::endl;
				} while (FindNextFile(hFind, &data));
				FindClose(hFind);
			}
		}
		else if (arguments[0].compare("secret") == 0)
		{
			HINSTANCE handle = LoadLibrary("c:\\Users\\User\\Desktop\\Secret.dll"); // Load library.
			if (!handle)
			{
				cerr << "Error loading current dll file: #" << GetLastError();
			}
			else
			{
				FARPROC ret = GetProcAddress(handle, "TheAnswerToLifeTheUniverseAndEverything"); // Load function.
				cout << "Returned value: " << ret();
			}
			FindClose(handle);

		}
		else if (arguments[0].compare("execute") == 0)
		{
			if (!WinExec(arguments[1].c_str(), SW_SHOWNORMAL))
			{
				cerr << "Error loading current exe. file: #" << GetLastError();
			}
		}
		cout << endl;
		cout << "> ";
	}
	system("PAUSE");
}